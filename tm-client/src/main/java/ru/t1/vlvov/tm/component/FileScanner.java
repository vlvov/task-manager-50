package ru.t1.vlvov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class FileScanner {

    private final Bootstrap bootstrap;

    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    private final List<String> commands = new ArrayList();

    private final File folder = new File(".\\");

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void process() {
        for (final File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            final String fileName = file.getName();
            if (commands.contains(fileName)) {
                file.delete();
                bootstrap.processCommand(fileName, false);
            }
        }
    }

    public void stop() {
        es.shutdown();
    }

    public void start() {
        Iterable<AbstractCommand> commands = bootstrap.getCommandRepository().getCommandsWithArgument();
        commands.forEach(e -> this.commands.add(e.getName()));
        es.scheduleAtFixedRate(this::process, 0, 5, TimeUnit.SECONDS);
    }

}
