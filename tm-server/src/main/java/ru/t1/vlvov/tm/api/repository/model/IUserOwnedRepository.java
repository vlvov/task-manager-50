package ru.t1.vlvov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.Sort;
import ru.t1.vlvov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void add(@NotNull final String userId, @NotNull M model);

    void update(@NotNull final String userId, @NotNull M model);

    void clear(@NotNull final String userId);

    @Nullable
    List<M> findAll(@NotNull final String userId);

    @Nullable
    M findOneById(@NotNull final String userId, @NotNull String id);

    void remove(@NotNull final String userId, @NotNull M model);

    void removeById(@NotNull final String userId, @NotNull String id);

    boolean existsById(@NotNull final String userId, @NotNull String id);

    @Nullable
    List<M> findAll(@NotNull final String userId, @NotNull Sort sort);

}
