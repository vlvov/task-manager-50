package ru.t1.vlvov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.repository.dto.IRepositoryDTO;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.dto.IServiceDTO;
import ru.t1.vlvov.tm.dto.model.AbstractModelDTO;
import ru.t1.vlvov.tm.exception.entity.EntityNotFoundException;
import ru.t1.vlvov.tm.exception.field.IdEmptyException;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public abstract class AbstractServiceDTO<M extends AbstractModelDTO, R extends IRepositoryDTO<M>> implements IServiceDTO<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractServiceDTO(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected abstract IRepositoryDTO<M> getRepository(@NotNull final EntityManager entityManager);

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepositoryDTO<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void add(@Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepositoryDTO<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepositoryDTO<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void set(@NotNull Collection<M> collection) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepositoryDTO<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.set(collection);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public List<M> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepositoryDTO<M> repository = getRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public M findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepositoryDTO<M> repository = getRepository(entityManager);
            return repository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepositoryDTO<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        M model = findOneById(id);
        remove(model);
    }

    @Override
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(id) != null;
    }

}
