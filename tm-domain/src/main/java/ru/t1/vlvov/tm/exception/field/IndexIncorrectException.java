package ru.t1.vlvov.tm.exception.field;

public final class IndexIncorrectException extends AbstractFieldNotFoundException {

    public IndexIncorrectException() {
        super("Error! Incorrect index...");
    }

}